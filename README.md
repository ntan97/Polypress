# Polypress
A basic theme template to integrate Polymer 1.0 to WordPress. This is a work in progress, and any help and pull requests will be accepted

What this is:
- A work in progress.
- A failure in progress.
- An attempt to combine some of the amazing Polymer APIS and Google things, with all the beautiful WordPress things we all know and love.

What this is not:
- A finished product, in an way, shape or form.

What are the goals for this?
- Create a skeleton theme that includes all the default styles and functionality of Polymer directly intergrated with WordPress
- Build off of and continuously improve as both WordPress and Polymer advance.

Why?
- Why not.

Whats Next?
- Add in all basic theme files, and create navigation options
- Integrate with different Google APIs.
- Figure out what's next after what's next.
